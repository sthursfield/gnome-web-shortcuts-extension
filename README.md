GNOME Web Shortcuts extension
=============================

This is a small project to improve support for 🔗 URL shortcuts in GNOME.

It adds a "Create web link..." menu item to the [file
manager](https://wiki.gnome.org/action/show/Apps/Files), which makes it simple
to create URL shortcut files in directories.

![Screenshot of Create Web Link dialog](docs/images/create-web-link-nautilus.png)

The shortcut format is defined by the
[Desktop Entry Specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html).
You can open these `.desktop` files with Firefox (on
[Linux platforms](https://bugzilla.mozilla.org/show_bug.cgi?id=442930)).

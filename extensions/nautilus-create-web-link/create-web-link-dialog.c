/*
 *  Nautilus Create Web Link
 *
 *  Copyright (C) 2019 Sam Thursfield
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Sam Thursfield <sam@afuera.me.uk>
 *
 */

#include <config.h>
#include <glib/gi18n-lib.h>

#include "create-web-link-dialog.h"

/* Called when one of the dialog's action buttons are clicked. */
static void
dialog_response (GtkDialog *dialog, int response_id, gpointer user_data) {
    CreateWebLinkDialogSuccessCallback *success_cb;
    GFile *directory;
    GtkWidget *name_entry, *url_entry;
    const char *name, *url;
    char *directory_path;

    success_cb = (CreateWebLinkDialogSuccessCallback *) user_data;

    if (response_id == GTK_RESPONSE_OK) {
        directory = g_object_get_data (G_OBJECT (dialog), "directory");
        name_entry = g_object_get_data (G_OBJECT (dialog), "name-entry");
        url_entry = g_object_get_data (G_OBJECT (dialog), "url-entry");

        directory_path = g_file_get_path (directory);
        name = gtk_entry_get_text (GTK_ENTRY (name_entry));
        url = gtk_entry_get_text (GTK_ENTRY (url_entry));

        success_cb (directory_path, name, url);

        g_free (directory_path);
    }
};

void
update_dialog_button_sensitivity (GtkWidget *dialog)
{
    GtkWidget *name_entry, *url_entry;
    gboolean sensitive;

    name_entry = g_object_get_data (G_OBJECT (dialog), "name-entry");
    url_entry = g_object_get_data (G_OBJECT (dialog), "url-entry");

    sensitive = (gtk_entry_get_text_length (GTK_ENTRY (name_entry)) > 0 && gtk_entry_get_text_length (GTK_ENTRY (url_entry)) > 0);

    gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), GTK_RESPONSE_OK, sensitive);
}

/* Returns a GtkDialog that prompts the user to create a web link. */
GtkWidget *
create_web_link_dialog_new (GtkWindow *parent_window,
                            const char *directory_uri,
                            CreateWebLinkDialogSuccessCallback *success_cb,
                            GError **error)
{
    GtkBuilder *builder;
    GtkWidget *dialog;
    GtkWidget *name_entry, *url_entry;
    GError *inner_error = NULL;

    builder = gtk_builder_new ();
    gtk_builder_set_translation_domain (builder, GETTEXT_PACKAGE);

    gtk_builder_add_from_resource (builder, "/org/gnome/gitlab/sthursfield/gnome-web-shortcuts-extension/nautilus-create-web-link/create-web-link-dialog.ui", &inner_error);

    if (inner_error) {
        g_propagate_error (error, inner_error);
        return NULL;
    }

    dialog = GTK_WIDGET (gtk_builder_get_object (builder, "create-web-link-dialog"));
    name_entry = GTK_WIDGET (gtk_builder_get_object (builder, "name"));
    url_entry = GTK_WIDGET (gtk_builder_get_object (builder, "url"));

    gtk_window_set_title (GTK_WINDOW (dialog), _("Create web link…"));
    gtk_window_set_transient_for (GTK_WINDOW (dialog), parent_window);

    g_object_set_data_full (G_OBJECT (dialog), "directory", g_file_new_for_uri (directory_uri), g_object_unref);
    g_object_set_data (G_OBJECT (dialog), "name-entry", name_entry);
    g_object_set_data (G_OBJECT (dialog), "url-entry", url_entry);

    update_dialog_button_sensitivity (dialog);
    g_signal_connect_swapped (name_entry, "notify::text-length", G_CALLBACK (update_dialog_button_sensitivity), dialog);
    g_signal_connect_swapped (url_entry, "notify::text-length", G_CALLBACK (update_dialog_button_sensitivity), dialog);

    g_object_unref (builder);

    g_signal_connect (dialog, "response", G_CALLBACK (dialog_response), success_cb);

    return dialog;
}

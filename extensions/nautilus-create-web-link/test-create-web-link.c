/*
 *  Nautilus Create Web Link
 *
 *  Copyright (C) 2019 Sam Thursfield
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Sam Thursfield <sam@afuera.me.uk>
 *
 */

#include <config.h>
#include <unistd.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>

#include "create-web-link.h"
#include "create-web-link-dialog.h"

void
dialog_cb (const char *directory_path, const char *name, const char *url) {
    create_web_link (directory_path, name, url);
}

int main (int argc, char **argv)
{
    GFile *directory;
    GtkWidget *dialog;
    GError *error = NULL;
    char buffer[PATH_MAX];

    bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
    bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
    textdomain (GETTEXT_PACKAGE);

    gtk_init (&argc, &argv);

    getcwd (buffer, PATH_MAX);
    directory = g_file_new_for_path (buffer);

    dialog = create_web_link_dialog_new (NULL, g_file_get_uri (directory), dialog_cb, &error);

    if (dialog == NULL) {
        g_critical ("Unable to create dialog: %s", error->message);
        return -1;
    }

    gtk_widget_show_all (dialog);

    gtk_dialog_run (GTK_DIALOG (dialog));

    gtk_widget_destroy (dialog);
    g_object_unref (directory);

    return 0;
}

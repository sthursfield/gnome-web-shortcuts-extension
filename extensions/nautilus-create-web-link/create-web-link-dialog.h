/*
 *  Nautilus Create Web Link
 *
 *  Copyright (C) 2019 Sam Thursfield
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Sam Thursfield <sam@afuera.me.uk>
 *
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef void (CreateWebLinkDialogSuccessCallback) (const char *directory_path, const char *name, const char *url);

GtkWidget *create_web_link_dialog_new (GtkWindow *parent_window, const char *directory_uri, CreateWebLinkDialogSuccessCallback *success_cb, GError **error);

G_END_DECLS

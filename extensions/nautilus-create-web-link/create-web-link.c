/*
 *  Nautilus Create Web Link
 *
 *  Copyright (C) 2019 Sam Thursfield
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Sam Thursfield <sam@afuera.me.uk>
 *
 */

#include <glib.h>

static gchar *
make_filename_for_link (const char *path, const char *name)
{
    GString *filename;

    filename = g_string_new (path);
    g_string_append_c (filename, G_DIR_SEPARATOR);
    g_string_append_uri_escaped (filename, name, " ()", TRUE);
    g_string_append (filename, ".desktop");

    return g_string_free (filename, FALSE);
}

/* Create a file inside directory 'path' that links to a website's URL.
 *
 * This function creates an XDG .desktop file of type "Link".
 *
 * See the XDG desktop file spec for details of the format:
 * <https://specifications.freedesktop.org/desktop-entry-spec/latest/1.2>.
 */
void
create_web_link (const char *path, const char *name, const char *url)
{
    GKeyFile *f;
    char *filename;
    GError *error = NULL;

    f = g_key_file_new ();

    g_key_file_set_string (f, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, "Link");
    g_key_file_set_string (f, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, name);
    g_key_file_set_string (f, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_URL, url);

    filename = make_filename_for_link (path, name);
    g_key_file_save_to_file (f, filename, &error);

    if (error != NULL) {
        g_warning ("Unable to save to file %s: %s", filename, error->message);
    }

    g_free (filename);
    g_key_file_free (f);
};

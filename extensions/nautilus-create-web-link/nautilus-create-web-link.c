/*
 *  Nautilus Create Web Link
 *
 *  Copyright (C) 2019 Sam Thursfield
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Sam Thursfield <sam@afuera.me.uk>
 *
 */

#include <config.h>
#include <string.h>
#include <glib/gi18n-lib.h>
#include <nautilus-extension.h>

#include "nautilus-create-web-link.h"
#include "create-web-link.h"
#include "create-web-link-dialog.h"

struct _NautilusCreateWebLink
{
    GObject parent_instance;
};

static void menu_provider_iface_init (NautilusMenuProviderInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (NautilusCreateWebLink, nautilus_create_web_link, G_TYPE_OBJECT, 0,
                                G_IMPLEMENT_INTERFACE_DYNAMIC (NAUTILUS_TYPE_MENU_PROVIDER,
                                                               menu_provider_iface_init))

typedef struct
{
    GtkWindow *parent_window;
    NautilusFileInfo *parent_folder;
} GetBackgroundItemsState;


GetBackgroundItemsState *
get_background_items_state_new (GtkWindow *parent_window,
                                NautilusFileInfo *parent_folder)
{
    GetBackgroundItemsState *state;

    state = g_slice_new0 (GetBackgroundItemsState);
    state->parent_window = g_object_ref (parent_window);
    state->parent_folder = g_object_ref (parent_folder);

    return state;
}

void get_background_items_state_free (GetBackgroundItemsState *state)
{
    g_object_unref (state->parent_window);
    g_object_unref (state->parent_folder);
    g_slice_free (GetBackgroundItemsState, state);
}


static void
dialog_success_cb (const char *directory_path,
                   const char *name,
                   const char *url)
{
    create_web_link (directory_path, name, url);
}

static void
create_web_link_callback (NautilusMenuItem *item,
                          gpointer          user_data)
{
    GetBackgroundItemsState *state;
    GtkWidget *dialog;
    GError *error = NULL;

    state = user_data;

    dialog = create_web_link_dialog_new (state->parent_window,
                                         nautilus_file_info_get_uri (state->parent_folder),
                                         dialog_success_cb,
                                         &error);

    if (dialog == NULL) {
        g_warning ("Unable to create dialog: %s", error->message);
    } else {
        gtk_dialog_run (GTK_DIALOG (dialog));

        gtk_widget_destroy (dialog);
    }
}

static GList *
get_background_items (NautilusMenuProvider *provider,
                      GtkWidget            *window,
                      NautilusFileInfo     *current_folder)
{
    GList *items = NULL;
    NautilusMenuItem *item;
    NautilusCreateWebLink *create_web_link;
    GetBackgroundItemsState *state;

    create_web_link = NAUTILUS_CREATE_WEB_LINK (provider);

    item = nautilus_menu_item_new ("NautilusCreateWebLink::create-web-link",
                                   _("Create web link…"),
                                   _("Send web link…"),
                                   "create-web-link");

    state = get_background_items_state_new (window, current_folder);

    g_signal_connect_data (item,
                           "activate",
                           G_CALLBACK (create_web_link_callback),
                           state,
                           (GClosureNotify) get_background_items_state_free,
                           0);

    items = g_list_append (items, item);

    return items;
}

static void
menu_provider_iface_init (NautilusMenuProviderInterface *iface)
{
    iface->get_background_items = get_background_items;
}

static void
nautilus_create_web_link_init (NautilusCreateWebLink *create_web_link)
{
}

static void
nautilus_create_web_link_class_init (NautilusCreateWebLinkClass *klass)
{
}

static void
nautilus_create_web_link_class_finalize (NautilusCreateWebLinkClass *klass)
{
}

void
nautilus_create_web_link_load (GTypeModule *module)
{
    nautilus_create_web_link_register_type (module);
}
